import { Hero } from './hero';
export class Results{
    count: number;
    next: string;
    previous: string;
    results: Hero[]
}
