import { Component, OnInit } from '@angular/core';
import { Hero} from '../hero';
import { RESULTS} from '../mock-heros';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
results = RESULTS;
hero: Hero = {
            name: 'Beru Whitesun lars', 
            height: '165', 
            mass: '75', 
            hair_color: 'brown', 
            skin_color: 'light', 
            eye_color: 'blue', 
            birth_year: '47BBY', 
            gender: 'female', 
            homeworld: 'https://swapi.co/api/planets/1/',
            films: [
                "https://swapi.co/api/films/2/", 
                "https://swapi.co/api/films/5/", 
                "https://swapi.co/api/films/4/", 
                "https://swapi.co/api/films/6/", 
                "https://swapi.co/api/films/3/", 
                "https://swapi.co/api/films/1/"
            ], 
            species: [
                "https://swapi.co/api/species/1/"
            ], 
            vehicles: [
                "https://swapi.co/api/vehicles/38/"
            ], 
            starships: [
                "https://swapi.co/api/starships/48/", 
                "https://swapi.co/api/starships/59/", 
                "https://swapi.co/api/starships/64/", 
                "https://swapi.co/api/starships/65/", 
                "https://swapi.co/api/starships/74/"
            ], 
            created: '2014-12-10T15:53:41.121000Z', 
            edited: '2014-12-20T21:17:50.319000Z', 
            url: 'https://swapi.co/api/people/7/'
}
  constructor() { }

  ngOnInit() {
  }

}
